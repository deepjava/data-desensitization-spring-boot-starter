# data-desensitization-spring-boot-starter

#### 介绍
简单的数据脱敏spring boot starter
适合用来学习如何自定义spring boot starter

#### 软件架构
自定义的spring boot starter
本项目用于自定义spring boot starter的学习  

#### 安装教程

1.  clone项目 设置maven仓库  clean  install安装到本地仓库

#### 使用说明

pom文件中引入

```pom
<dependency>
    <groupId>org.example</groupId>
    <artifactId>data-desensitization-spring-boot-starter</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```

在你需要脱敏的字段上使用注解 @Desensitization 并且设置 对应的类型即可 
例如下面代码是给用户的手机号脱敏 
```java
@Data
class User {
    @Desensitization(type = DesensitizationTypeEnum.MOBILE_PHONE)
    private String phone;
}
```

支持的脱敏类型

```
public enum DesensitizationTypeEnum {
    //自定义
    MY_RULE,
    //用户id
    USER_ID,
    //中文名
    CHINESE_NAME,
    //身份证号
    ID_CARD,
    //座机号
    FIXED_PHONE,
    //手机号
    MOBILE_PHONE,
    //地址
    ADDRESS,
    //电子邮件
    EMAIL,
    //密码
    PASSWORD,
    //中国大陆车牌，包含普通车辆、新能源车辆
    CAR_LICENSE,
    //银行卡
    BANK_CARD
}
```
支持自定义脱敏结构 MY_RULE
下面代码从第2个字符开始到第三个字符结束 也就是脱敏第二和第三个字符
```
@Desensitization(type = DesensitizationTypeEnum.MY_RULE, startInclude = 1, endExclude = 3)
private String xxx;
```

#### 配置说明

配置可选  如果不配置默认为true   当配置为false时脱敏功能关闭 

desensitization.enabled=true

