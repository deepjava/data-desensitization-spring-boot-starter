package org.example.config;

import lombok.extern.slf4j.Slf4j;
import org.example.base.DesensitizationSerialize;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 数据脱敏配置类
 */
@Configuration
@EnableConfigurationProperties(DesensitizationProperties.class)
@Slf4j
public class DesensitizationAutoConfiguration {

    @Bean
    public CommandLineRunner printLog(DesensitizationProperties desensitizationProperties) {
        DesensitizationSerialize.setEnabled(desensitizationProperties.getEnabled());
        return args -> log.info("是否开启数据脱敏：" + desensitizationProperties.getEnabled());
    }

}

