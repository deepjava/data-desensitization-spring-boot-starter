package org.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "desensitization")
public class DesensitizationProperties {
    private Boolean enabled = true;
}
